var camera, scene, renderer, INTERSECTED, ui, widget;
var box, plane, light, delta, controls, texture, bg, clock, raycaster, mouse, stats, container, player, face, coords;
init();
animate();
function init() {
    //RENDERER
    renderer = new THREE.WebGLRenderer();
    renderer.setSize( window.innerWidth, window.innerHeight );
    document.body.appendChild( renderer.domElement );

    //MISC
    clock = new THREE.Clock();
    raycaster = new THREE.Raycaster();
    raycaster.far = 1000;
    mouse = new THREE.Vector2();
    container = document.createElement('div');
    document.body.appendChild(container);
    container.appendChild(renderer.domElement);
    stats = new Stats();
    container.appendChild(stats.dom);

    //SCENE
    scene = new THREE.Scene();
    //scene.background = new THREE.Color(0xffffff);
    scene.background = new THREE.TextureLoader().load("resources/images/dark-space-texture.jpg");

    //CAMERA
    camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 0.1, 1000 );
    camera.position.set(0, 10, 25);

    controls = new CameraControls(camera, renderer.domElement);
    // controls.mouseEnabled = false;
    controls.movementSpeed = 20;

    //LIGHT
    light = new THREE.AmbientLight(0xdbeaff);

    scene.add(light);

    ui = new UIBuilder();
    widget = ui.positionWidget(camera.position, "camera");
    ui.makeCrosshair();

    player = new Player(camera.position);
    player.buildCube();

    window.addEventListener('resize', onWindowResize, false);
    window.addEventListener('mousedown', onMouseDown, false);
}

function onMouseDown(event) {
    switch (face) {
        case 'f':
            player.buildCube(coords.x, coords.y, coords.z + 2);
            break;
        case 'b':
            player.buildCube(coords.x, coords.y, coords.z - 2);
            break;
        case 'r':
            player.buildCube(coords.x + 2, coords.y, coords.z);
            break;
        case 'l':
            player.buildCube(coords.x - 2, coords.y, coords.z);
            break;
        case 'u':
            player.buildCube(coords.x, coords.y + 2, coords.z);
            break;
        case 'd':
            player.buildCube(coords.x, coords.y - 2, coords.z);
            break;
    }
}
function onWindowResize() {
    renderer.setSize( window.innerWidth, window.innerHeight );
    camera.aspect = window.innerWidth/ window.innerHeight;
    camera.updateProjectionMatrix();
}

function animate(){
    delta = clock.getDelta();
    ui.updatePositionWidget(widget, camera.position);
    raycaster.setFromCamera({x: 0, y: 0}, camera);

    var intersects = raycaster.intersectObjects( scene.children );

    if ( intersects.length > 0 ) {
        if ( INTERSECTED != intersects[ 0 ].object ) {
            if ( INTERSECTED ) INTERSECTED.material.emissive.setHex( INTERSECTED.currentHex );
            INTERSECTED = intersects[ 0 ].object;
            INTERSECTED.currentHex = INTERSECTED.material.emissive.getHex();
            INTERSECTED.material.emissive.setHex( 0x1f1f1f );
            face = player.findFace(INTERSECTED.geometry.vertices, raycaster.ray.direction);
            coords = INTERSECTED.position;
        }
    } else {
        if ( INTERSECTED ) INTERSECTED.material.emissive.setHex( INTERSECTED.currentHex );
        INTERSECTED = null;
    }

    controls.update(delta);
    stats.update();
    renderer.render(scene, camera);
    requestAnimationFrame(animate);
}
