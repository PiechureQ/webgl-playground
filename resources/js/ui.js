UIBuilder = function() {
    this.positionWidget = function(position, type){
        var posWidget = $("<div id='posWidget'></div>").text(type);
        var p1 = $("<p></p>").text(position.x);
        var p2 = $("<p></p>").text(position.y);
        var p3 = $("<p></p>").text(position.z);
        $(posWidget).append(p1, p2, p3);
        $("body").append(posWidget);
        return posWidget;
    }

    this.updatePositionWidget = function(widget, new_data){
        $(widget).children().empty();
        var p1 = $("<p></p>").text(new_data.x);
        var p2 = $("<p></p>").text(new_data.y);
        var p3 = $("<p></p>").text(new_data.z);
        $(widget).append(p1, p2, p3);
    }

    this.makeCrosshair = function() {
        var crosshair = $("<div id='crosshair'></div>").text(".");
        $("body").append(crosshair);
    };
};