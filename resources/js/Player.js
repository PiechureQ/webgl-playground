Player = function(object){
    this.position = object;

    this.defaultBox = new DefaultBox(2, 2, 2);
    let box;

    this.buildCube = function(x, y, z) {
        if(x == null && y == null && z == null) {
            box = this.defaultBox.getDefaultBox()
        }else{
            box = this.defaultBox.getBoxAtPosition(x, y, z);
        }
        scene.add(box);
    };

    this.findFace = function(vertices, raydirection){
        let front = findCenter(vertices[0], vertices[5], vertices[7], vertices[2]);
        let back = findCenter(vertices[4], vertices[1], vertices[3], vertices[6]);

        let right = findCenter(vertices[2], vertices[3], vertices[1], vertices[0]);
        let left = findCenter(vertices[7], vertices[5], vertices[4], vertices[6]);

        let up = findCenter(vertices[4], vertices[5], vertices[1], vertices[0]);
        let down = findCenter(vertices[7], vertices[2], vertices[3], vertices[6]);

        var facesPositions = [front, back, right, left, up, down];
        let face = compareDistances(facesPositions, raydirection);
        switch(face){
            case 0:
                return 'f';
            case 1:
                return 'b';
            case 2:
                return 'r';
            case 3:
                return 'l';
            case 4:
                return 'u';
            case 5:
                return 'd';
        }
    };

    function findCenter(v0, v1 ,v2 ,v3) {
        let v = v0.clone().add(v1).add(v2).add(v3);
        v.divide(new THREE.Vector3(4, 4, 4));
        return v;
    }

    function compareDistances(faces, ray) {
        let distances = [];
        for(var i = 0; i < 6;i++) {
            distances[i] = faces[i].dot(ray);
        }
        return distances.indexOf(Math.min(...distances));
    }
};

DefaultBox = function(geox, geoy, geoz) {
    this.geo = new THREE.BoxGeometry(geox, geoy, geoz);
    this.mat = new THREE.MeshToonMaterial({color: 0xffffff});

    this.getDefaultBox = function() {
        return new THREE.Mesh(this.geo.clone(), this.mat.clone());
    };

    this.getBoxAtPosition = function(x, y, z) {
        let transformMatrix = new THREE.Matrix4();
        transformMatrix.makeTranslation(x, y, z);
        let mesh = new THREE.Mesh(this.geo.clone(), this.mat.clone())
        mesh.position.applyMatrix4(transformMatrix);
        return mesh;
    };
};