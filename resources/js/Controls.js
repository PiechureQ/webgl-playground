CameraControls = function(camera, domElement) {
    this.camera = camera;
    this.domElement = domElement;

    this.movementSpeed = 100;
    this.mouseSensitivity = 10;

    var pitch = new THREE.Object3D();
    var yaw = new THREE.Object3D();
    var roll = new THREE.Object3D();

    this.moveForward = false;
    this.moveLeft = false;
    this.moveBackward = false;
    this.moveRight = false;
    this.moveUp = false;
    this.moveDown = false;
    this.rotateLeft = false;
    this.rotateRight = false;

    this.mouseLocked = false;
    this.mouseEnabled = true;

    this.onMouseMove = function(event) {
        if(this.mouseLocked && this.mouseEnabled) {
            var movementX = event.movementX || event.mozMovementX || event.webkitMovementX || 0;
            var movementY = event.movementY || event.mozMovementY || event.webkitMovementY || 0;

            yaw.rotation.y -= movementX * 0.0002 * this.mouseSensitivity;
            pitch.rotation.x -= movementY * 0.0002 * this.mouseSensitivity;
        }
    };

    this.onMouseDown = function() {
        if(this.mouseEnabled) {
            this.domElement.requestPointerLock = this.domElement.requestPointerLock || this.domElement.mozRequestPointerLock || this.domElement.webkitRequestPointerLock;
            if (!this.mouseLocked) this.domElement.requestPointerLock();
        }
    };

    this.onPointerLockChange = function() {
        this.mouseLocked = !this.mouseLocked;
    };

    this.onKeyDown = function ( event ) {
        switch ( event.keyCode ) {
            case 38: /*up*/
            case 87: /*W*/ this.moveForward = true; break;

            case 37: /*left*/
            case 65: /*A*/ this.moveLeft = true; break;

            case 40: /*down*/
            case 83: /*S*/ this.moveBackward = true; break;

            case 39: /*right*/
            case 68: /*D*/ this.moveRight = true; break;

            case 82: /*R*/ this.moveUp = true; break;
            case 70: /*F*/ this.moveDown = true; break;

            case 81: /*Q*/ this.rotateLeft = true; break;
            case 69: /*E*/ this.rotateRight = true; break;
        }
    };

    this.onKeyUp = function ( event ) {
        switch ( event.keyCode ) {
            case 38: /*up*/
            case 87: /*W*/ this.moveForward = false; break;

            case 37: /*left*/
            case 65: /*A*/ this.moveLeft = false; break;

            case 40: /*down*/
            case 83: /*S*/ this.moveBackward = false; break;

            case 39: /*right*/
            case 68: /*D*/ this.moveRight = false; break;

            case 82: /*R*/ this.moveUp = false; break;
            case 70: /*F*/ this.moveDown = false; break;

            case 81: /*Q*/ this.rotateLeft = false; break;
            case 69: /*E*/ this.rotateRight = false; break;
        }
    };

    this.update = function(delta) {
        var movementSpeed = delta * this.movementSpeed;

        var direction = new THREE.Vector3();
        var rotation = new THREE.Euler(0, 0, 0, 'YXZ');

        direction.z = Number(this.moveBackward) - Number(this.moveForward);
        direction.x = Number(this.moveRight) - Number(this.moveLeft);
        direction.y = Number(this.moveUp) - Number(this.moveDown);

        var rollDir = Number(this.rotateLeft) - Number(this.rotateRight);
        roll.rotation.z += rollDir * movementSpeed * 0.02;
        direction.normalize();

        rotation.set(pitch.rotation.x, yaw.rotation.y, roll.rotation.z);

        this.camera.translateX(direction.x * movementSpeed);
        this.camera.translateY(direction.y * movementSpeed);
        this.camera.translateZ(direction.z * movementSpeed);

        this.camera.setRotationFromEuler(rotation);
    };

    var onKeyDown = bind(this, this.onKeyDown);
    var onKeyUp = bind(this, this.onKeyUp);
    var onMouseMove = bind(this, this.onMouseMove);
    var onMouseDown = bind(this, this.onMouseDown);
    var onPointerLockChange = bind(this, this.onPointerLockChange);

    function bind( scope, fn ) {
        return function () {
            fn.apply( scope, arguments );
        };
    }

    window.addEventListener('keydown', onKeyDown, false);
    window.addEventListener('keyup', onKeyUp, false);
    window.addEventListener('mousemove', onMouseMove, false);
    window.addEventListener('mousedown', onMouseDown, false);
    window.addEventListener('pointerlockchange', onPointerLockChange, false);
};