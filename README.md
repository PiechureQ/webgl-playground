# WebGL Playground

Each branch represents diffrent expreriments.

### Experiments

 - `shader` GLSL fragment shader that represents a change in Euler's line depending on triangle vertices
 - `threejs-example` Simple "game" that I wrote to learn three.js
 - `transfrom` 2D transformations on the squere
 - `webgl` First approach to 3D rendering with WebGL 

### Runing

Just run the http server in project directory, or open index.html in a web browser. 

License
----

MIT
